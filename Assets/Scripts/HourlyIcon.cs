using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HourlyIcon : MonoBehaviour
{
    public Image Icon;
    public RawImage Column;
    public TextMeshProUGUI HourText;
    public TextMeshProUGUI HourTempText;
    public int Hour = 0;
    public int Temperature = 0;
    public CanvasGroup CanvasGroup;

}
